<?php

if($_POST){

  require_once  'lib/funciones.php';

  $fechainicial = explode('-',$_POST['fi']);
  $fechafinal = explode('-',$_POST['ff']);
  $autor = $_POST['author'];
  $redes = $_POST['redes'];
  $cantidad = $_POST['cantidad'];

  $args = array(
    'date_query' => array(
      array(
        'after'     => array(
          'year'  => $fechainicial[0],
          'month' => $fechainicial[1],
          'day'   => $fechainicial[2],
        ),
        'before'    => array(
          'year'  => $fechafinal[0],
          'month' => $fechafinal[1],
          'day'   => $fechafinal[2],
        ),
        'inclusive' => true,
      )
    ));

  if($autor!='-1'):
    $args['author'] = $autor;
  endif;


    $query = new WP_Query ($args);
    $lista = [];
    $filas = [];
    $i = 0;
    $j = 10;
  if ($query->have_posts()) : while ($query->have_posts()) :
        $query->the_post();
        if($redes==0){
          $social_f = share_count(get_the_id(), 'facebook');
          $social_t = share_count(get_the_id(), 'twitter');
          $social_g = share_count(get_the_id(), 'google-plus');
          $compartida = $social_f + $social_t +$social_g;
        }
        else{
          $compartida = share_count(get_the_id(), $redes);
        }

        $filas[$i] = array(
            'id' => get_the_id(),
            'titulo' => get_the_title(),
            'autor' => get_the_author(),
            'fecha' => get_the_date(),
            'compartida' => $compartida,
            'link' => get_the_permalink()
        );

        $i++;
        $j++;
      endwhile;
  wp_reset_query();
  endif;

  foreach ($filas as $key => $row) {
      $aux[$key] = $row['compartida'];
  }

  array_multisort($aux, SORT_DESC, $filas);
  require_once  'view/view_informe.php';


<table class="wp-list-table widefat fixed striped posts" style="margin-top:50px;">
        <thead>
        <tr>
          <th class="manage-column">TITULO</th>
          <th class="manage-column">AUTOR</th>
          <th class="manage-column">Fecha</th>
          <th class="manage-column">Compartida</th>
          <th class="manage-column">Enlace</th>
        </tr>
        </thead>

      <?php
        $recorrido = 1;
        foreach ($filas as $key => $row) {
            if($recorrido<=$cantidad){
      ?>
            <tr>
              <td class="manage-column"><?php echo $row['titulo'];?></td>
              <td class="manage-column"><?php echo $row['autor'];?></td>
              <td class="manage-column"><?php echo $row['fecha'];?></td>
              <td class="manage-column"><?php echo $row['compartida'];?></td>
              <td class="manage-column"><a href="<?php echo $row['link'];?>" target="_blank">Link</a></td>
            </tr>
      <?php
          }
          $recorrido++;
        }
      ?>
      </tbody>
</table>


<?php
} ?>
