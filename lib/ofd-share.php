<?php

if (!function_exists('SocialShare_Networks')) {
	function SocialShare_Networks($sites = array())
	{

		$SocialShare_Networks = [
			'facebook' => [
				'name' => 'Facebook',
				'class' => 'facebook',
				'fontawesome' => 'facebook-official',
				'url' => 'https://www.facebook.com/sharer.php?s=100&p[url]={url}&p[images][0]={img}&p[title]={title}&p[summary]={desc}'
			],
			'facebook-dialog' => [
				'name' => 'Facebook (share dialog)',
				'class' => 'facebook',
				'fontawesome' => 'facebook',
				'url' => 'https://www.facebook.com/dialog/share?app_id={app_id}&display=page&href={url}&redirect_uri={redirect_url}'
			],
			'twitter' => [
				'name' => 'Twitter',
				'class' => 'twitter',
				'fontawesome' => 'twitter',
				'url' => 'https://twitter.com/intent/tweet?url={url}&text={title}&via={via}&hashtags={hashtags}'
			],
			'whatsapp' => [
				'name' => 'WhatsApp',
				'class' => 'whatsapp',
				'fontawesome' => 'whatsapp',
				'target' => false,
				'url' => 'whatsapp://send?text={url}',
			],
			'copy' => [
				'name' => 'Copiar',
				'class' => 'ss-copy',
				'fontawesome' => 'clone',
				'target' => false,
				'url' => ''
			],
			'email' => [
				'name' => 'Email',
				'class' => 'email',
				'fontawesome' => 'envelope-o',
				'target' => false,
				'url' => 'mailto:?subject={title}&body={desc}. {url}'
			],
			'google' => [
				'name' => 'Google+',
				'class' => 'google',
				'fontawesome' => 'google',
				'url' => 'https://plus.google.com/share?url={url}',
			],
			'pinterest' => [
				'name' => 'Pinterest',
				'class' => 'pinterest',
				'fontawesome' => 'pinterest',
				'url' => 'https://pinterest.com/pin/create/bookmarklet/?media={img}&url={url}&is_video={is_video}&description={title}',
			],
			'linkedin' => [
				'name' => 'Linked In',
				'class' => 'linkedin',
				'fontawesome' => 'linkedin',
				'url' => 'https://www.linkedin.com/shareArticle?url={url}&title={title}',
			],
			'buffer' => [
				'name' => 'Buffer',
				'class' => 'buffer',
				'fontawesome' => 'buffer',
				'url' => 'https://buffer.com/add?text={title}&url={url}',
			],
			'digg' => [
				'name' => 'Digg',
				'class' => 'digg',
				'fontawesome' => 'digg',
				'url' => 'http://digg.com/submit?url={url}&title={title}',
			],
			'tumblr' => [
				'name' => 'Tumblr',
				'class' => 'tumblr',
				'fontawesome' => 'tumblr',
				'url' => 'https://www.tumblr.com/widgets/share/tool?canonicalUrl={url}&title={title}&caption={desc}',
			],
			'reddit' => [
				'name' => 'Reddit',
				'class' => 'reddit',
				'fontawesome' => 'reddit',
				'url' => 'https://reddit.com/submit?url={url}&title={title}',
			],
			'stumbleupon' => [
				'name' => 'StumbleUpon',
				'class' => 'stumbleupon',
				'fontawesome' => 'stumbleupon',
				'url' => 'http://www.stumbleupon.com/submit?url={url}&title={title}',
			],
			'delicious' => [
				'name' => 'Delicious',
				'class' => 'delicious',
				'fontawesome' => 'delicious',
				'url' => 'https://del.icio.us/save?v=5&provider={provider}&noui&jump=close&url={url}&title={title}',
			],
			'blogger' => [
				'name' => 'Blogger',
				'class' => 'blogger',
				'fontawesome' => 'blogger',
				'url' => 'https://www.blogger.com/blog-this.g?u={url}&n={title}&t={desc}',
			],
			'livejournal' => [
				'name' => 'LiveJournal',
				'class' => 'livejournal',
				'fontawesome' => 'livejournal',
				'url' => 'http://www.livejournal.com/update.bml?subject={title}&event={url}',
			],
			'myspace' => [
				'name' => 'MySpace',
				'class' => 'myspace',
				'fontawesome' => 'myspace',
				'url' => 'https://myspace.com/post?u={url}&t={title}&c=desc',
			],
			'yahoo' => [
				'name' => 'Yahoo',
				'class' => 'yahoo',
				'fontawesome' => 'yahoo',
				'url' => 'http://compose.mail.yahoo.com/?body={url}',
			],
			'friendfeed' => [
				'name' => 'FriendFeed',
				'class' => 'friendfeed',
				'fontawesome' => 'friendfeed',
				'url' => 'http://friendfeed.com/?url={url}',
			],
			'newsvine' => [
				'name' => 'NewsVine',
				'class' => 'newsvine',
				'fontawesome' => 'newsvine',
				'url' => 'http://www.newsvine.com/_tools/seed&save?u={url}',
			],
			'evernote' => [
				'name' => 'EverNote',
				'class' => 'evernote',
				'fontawesome' => 'evernote',
				'url' => 'http://www.evernote.com/clip.action?url={url}',
			],
			'getpocket' => [
				'name' => 'GetPocket',
				'class' => 'getpocket',
				'fontawesome' => 'getpocket',
				'url' => 'https://getpocket.com/save?url={url}',
			],
			'flipboard' => [
				'name' => 'FlipBoard',
				'class' => 'flipboard',
				'fontawesome' => 'flipboard',
				'url' => 'https://share.flipboard.com/bookmarklet/popout?v=2&title={title}&url={url}',
			],
			'instapaper' => [
				'name' => 'InstaPaper',
				'class' => 'instapaper',
				'fontawesome' => 'instapaper',
				'url' => 'http://www.instapaper.com/edit?url={url}&title={title}&description={desc}',
			],
			'sms' => [
				'name' => 'SMS',
				'class' => 'sms',
				'fontawesome' => 'mobile',
				'target' => false,
				'url' => 'sms:?body={title}. {url}'
			],

			'skype' => [
				'name' => 'Skype',
				'class' => 'skype',
				'fontawesome' => 'skype',
				'url' => 'https://web.skype.com/share?url={url}',
			],
			'viber' => [
				'name' => 'Viber',
				'class' => 'viber',
				'fontawesome' => 'viber',
				'url' => 'viber://forward?text={url}',
			],
			'telegramme' => [
				'name' => 'Telegram.me',
				'class' => 'telegramme',
				'fontawesome' => 'telegramme',
				'url' => 'https://telegram.me/share/url?url={url}&text={title}',
			],
			'vk' => [
				'name' => 'VK',
				'class' => 'vk',
				'fontawesome' => 'vk',
				'url' => 'http://oauth.vk.com/authorize?client_id=-1&redirect_uri={url}&display=widget&caption={title}',
			],
			'okru' => [
				'name' => 'OK.ru',
				'class' => 'okru',
				'fontawesome' => 'okru',
				'url' => 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl={url}&title={title}',
			],
			'douban' => [
				'name' => 'Douban',
				'class' => 'douban',
				'fontawesome' => 'douban',
				'url' => 'http://www.douban.com/recommend/?url={url}&title={title}',
			],
			'baidu' => [
				'name' => 'Baidu',
				'class' => 'baidu',
				'fontawesome' => 'baidu',
				'url' => 'http://cang.baidu.com/do/add?it={title}&iu={url}',
			],
			'qzone' => [
				'name' => 'QZone',
				'class' => 'qzone',
				'fontawesome' => 'qzone',
				'url' => 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url={url}',
			],
			'xing' => [
				'name' => 'Xing',
				'class' => 'xing',
				'fontawesome' => 'xing',
				'url' => 'https://www.xing.com/app/user?op=share&url={url}',
			],
			'renren' => [
				'name' => 'RenRen',
				'class' => 'renren',
				'fontawesome' => 'renren',
				'url' => 'http://widget.renren.com/dialog/share?resourceUrl={url}&srcUrl={url}&title={title}',
			],
			'weibo' => [
				'name' => 'Weibo',
				'class' => 'weibo',
				'fontawesome' => 'weibo',
				'url' => 'http://service.weibo.com/share/share.php?url={url}&appkey=&title={text}&pic=&ralateUid=',
			],
		];

		if (count($sites) > 0) {
			$tmp = array();
			foreach ($sites as $site) {
				if (isset($SocialShare_Networks[$site])) {
					$tmp[$site] = $SocialShare_Networks[$site];
				}
			}
			if (count($tmp) > 0) {
				$SocialShare_Networks = $tmp;
			}
		}

		return $SocialShare_Networks;
	}
}


if (!function_exists('SocialShare_generateSocialUrls')) {
	function SocialShare_generateSocialUrls($sites = array(), $options = array(), $single = false)
	{
		global $post;
		$_options = [
			'url' => get_the_permalink(),
			'img' => get_the_post_thumbnail_url(get_the_ID()),
			'title' => get_the_title(),
			'desc' => cmax_the_excerpt(),
			'redirect_url' => get_the_permalink(),
		];
		$options = $_options;

		if (in_array('facebook', $sites)) {
			$app_id = '';
			if ($app_id != "") {
				$options['app_id'] = $app_id;
				if (($key = array_search('facebook', $sites)) !== false) {
					unset($sites[$key]);
				}
				$sites[] = 'facebook-dialog';
			}
			$options[''] = '';
		}

		$links = array();
		$networks = SocialShare_Networks($sites);
		foreach ($networks as $network_key => $network) {
			$_ss_url = SocialShare_generateUrl($network['url'], $options, $network_key);

			if (in_array($network_key, ['copy'])) {
				$_ss_url = $options['url'];
			}

			array_push($links, [
				'name' => $network['name'],
				'class' => $network['class'],
				'fontawesome' => $network['fontawesome'],
				'target' => (isset($network['target']) && ($network['target'] === false)) ? '  data-target="off" ' : ' target="_blank" ',
				'url' => $_ss_url,
			]);
		}

		if ($single) {
			return $links[0];
		}

		return $links;
	}
}



if (!function_exists('SocialShare_generateUrl'))
{
	function SocialShare_generateUrl($url = '', $options = array(), $network = null)
	{
		foreach ($options as $prop_key => $prop)
		{
			$arg = '{' . $prop_key . '}';
			if (strpos($url, $arg) !== false)
			{
				if (($network == 'sms'))
				{
					if (in_array($prop_key, ['title', 'url'])) {
						$url = str_replace($arg, $prop, $url);
					}
				}
				else {
					$url = str_replace($arg, urlencode($prop), $url);
				}
			}
		}
		return SocialShare_cleanUrl($url);
	}
}


if (!function_exists('SocialShare_cleanUrl')) {
	function SocialShare_cleanUrl($fullUrl = '')
	{
		$fullUrl = preg_replace('/\{([^{}]*)}/', '', $fullUrl);
		$url_array = parse_url($fullUrl);
		$url = explode('?', $fullUrl)[0];
		$query_data_parts = array();
		$params_tmp = isset($url_array['query']) ? explode('&', $url_array['query']) : array();
		if (!empty($params_tmp) > 0) {
			foreach ($params_tmp as $param_tmp) {
				$query_parts = explode('=', $param_tmp);
				if (count($query_parts) > 0) {
					if (!empty($query_parts[1])) {
						$query_data_parts[] = implode('=', $query_parts);
					}
				}
			}
		}
		$params = implode('&', $query_data_parts);
		if (strlen($params) > 0) {
			$url .= "?" . $params;
		}
		return ($url);
	}
}
