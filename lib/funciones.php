<?php


// funcion  para consulta cuantas veces fue compartida una url de nuestra pagina.
function share_count( $post_id, $network_slug ) {

    if( !isset( $post_id ) && !isset( $network_slug ) )
      return false;

    // Get page url for the post
    $page_url = get_permalink( $post_id );
    $page_url = urlencode( $page_url );

    // Default post arguments
    $args = array( 'timeout' => 60 );
    $app = '';
    // Prepare urls to get remote request
    switch( $network_slug ) {

      case 'facebook':
        if( empty( $app ) )
          $url = 'https://graph.facebook.com/?id=' . $page_url;
        else
          $url = 'https://graph.facebook.com/v2.7/?id=' . $page_url . '&access_token=' . $app;
        break;

      case 'twitter':
        $url = 'http://public.newsharecounts.com/count.json?url=' . $page_url ;
        break;

      case 'google-plus':

        $args = array(
          'timeout' => 60,
          'headers' => 'Content-type: application/json\r\n',
          'body' => json_encode(
            array(
              'method' => 'pos.plusones.get',
              'id' => 'p',
              'params' => array(
                'nolog' => true,
                'id' => urldecode( $page_url ),
                'source' => 'widget',
                'userId' => '@viewer',
                'groupId' => '@self'
              ),
              'jsonrpc' => '2.0',
              'key' => 'p',
              'apiVersion' => 'v1'
            )
          )
        );

        $url = 'https://clients6.google.com/rpc/?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ';
        break;

      case 'pinterest':
        $url = 'http://widgets.pinterest.com/v1/urls/count.json?source=6&url=' . $page_url;
        break;

    }

    // Get response from the api call
    if( $network_slug == 'google-plus' )
      $response = wp_remote_post( $url, $args );
    else
      $response = wp_remote_get( $url, $args );


    // Continue only if response code is 200
    if( wp_remote_retrieve_response_code( $response ) == 200 ) {

      $body = json_decode( wp_remote_retrieve_body( $response ), true );

      // Get share value from response body
      switch( $network_slug ) {

        case 'facebook':
          $share_count = isset( $body['share']['share_count'] ) ? $body['share']['share_count'] : false;
          break;

        case 'google-plus':
          $share_count = isset( $body['result']['metadata']['globalCounts']['count'] ) ? (int)$body['result']['metadata']['globalCounts']['count'] : false;
          break;

        case 'pinterest':
          $body 	= wp_remote_retrieve_body( $response );
          $start  = strpos( $body, '(' );
          $end    = strpos( $body, ')', $start + 1 );
          $length = $end - $start;
          $body 	= json_decode( substr( $body, $start + 1, $length - 1 ), true );

          $share_count = ( isset( $body['count'] ) ? $body['count'] : false );

          break;

        default:
          $share_count = ( isset( $body['count'] ) ? $body['count'] : false );
          break;

      }

      return ( $share_count ? (int)$share_count : $share_count );

    } else {

      return 0;

    }

    return 0;

  }


  function number_shorten( $number, $precision = 3, $divisors = null ) {

  	if ( ! isset( $divisors ) ) {
  		$divisors = array(
  			pow( 1000, 0 ) => '', // 1000^0 == 1
  			pow( 1000, 1 ) => 'K', // Thousand
  			pow( 1000, 2 ) => 'M', // Million
  			pow( 1000, 3 ) => 'B', // Billion
  			pow( 1000, 4 ) => 'T', // Trillion
  			pow( 1000, 5 ) => 'Qa', // Quadrillion
  			pow( 1000, 6 ) => 'Qi', // Quintillion
  		);
  	}

  	foreach ( $divisors as $divisor => $shorthand ) {
  		if ( abs( $number ) < ( $divisor * 1000 ) ) {
  			break;
  		}
  	}

  	return number_format( $number / $divisor ) . $shorthand;
  }



 ?>
