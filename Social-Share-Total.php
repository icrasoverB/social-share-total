<?php
/**
* Plugin Name: Social Share Total
* Plugin URI: https://itsoft.com
* Description: Informe social
* Version: 1.0.0
* Author: CG. BKC
* Author URI: http://itsoft.com
* License: GPL2
*/


function itsoft_page_menu(){
    add_menu_page(
    	'Informe social',
    	'Informe Social',
    	'publish_posts',
    	'theme-options',
    	'page_index',
    	'dashicons-book-alt',
    	10);
}

add_action('admin_menu', 'itsoft_page_menu');



add_action('admin_init', 'enqueue_my_styles');

function wp_body_classes( $classes ) {
    $classes[] = 'class-name';

    return $classes;
}
add_filter( 'body_class','wp_body_classes' );


function enqueue_my_styles(){
  $version = date('Ymdigh');

  wp_enqueue_script('jquery-ui-core');
  wp_enqueue_script('jquery-ui-datepicker');
  wp_enqueue_style('jquery-ui-css', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

  wp_enqueue_script( 'plu-chart', plugin_dir_url(__FILE__) .'assets/js/plu-chart.js',false,'1.7',true);
  wp_enqueue_script( 'plu-main',  plugin_dir_url(__FILE__) .'assets/js/plu-main.js', array( 'jquery' ),$version);
	wp_localize_script('plu-main', 'plu_admin_ajax', admin_url('admin-ajax.php'));

  wp_enqueue_style('plu-style',plugin_dir_url(__FILE__) .'assets/css/plu-main.css','all',$version);//la ruta de nuestro css
  wp_enqueue_style('plus-fontawesome',plugin_dir_url(__FILE__) .'assets/css/font-wesome-4.6.3/font-awesome.min.css');//la ruta de nuestro css
}



/** Ajax filtro */
add_action( 'wp_ajax_filtro_ajax', 'filtro_ajax_init' );
add_action( 'wp_ajax_nopriv_filtro_ajax', 'filtro_ajax_init' );

function filtro_ajax_init() {

  require_once  'lib/funciones.php';
  require_once  'lib/ofd-share.php';

  $fechainicial = explode('-',$_GET['fi']);
  $fechafinal = explode('-',$_GET['ff']);
  $autor = $_GET['author'];
  $redes = $_GET['redes'];
  $cantidad = $_GET['cantidad'];
  $tipo = $_GET['tipo'];
  $cat          = $_GET['cat'];

  require_once  'consulta.php';


  $html = '';
  $recorrido = 1;
  $i=1;
  foreach ($filas as $key => $row) {
      if($recorrido<=$cantidad){
        $total = number_shorten($row['totaltipo']);

        $html.='<tr>
          <td class="plus-flex c-body">
            <img src="'.$row['img'].'" width="60" height="60" style="display: inline-block;padding-right: 10px;">
            <p>
              <a href="'.$row['link'].'" data-id="'.$row['id'].'" target="_blank">'.$row['titulo'].'</a>
              <span>'.$row['autor'].' | '.$row['fecha'].'</span>
            </p>
          </td>
          <td style="padding-top:15px;"><span class="total-share">'.$total.'</span></td>
          <td class="c-body">
            <a href="'.$row['ss_share_fb'].'" target="_blank" class="plu-s facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="'.$row['ss_share_tw'].'" target="_blank" class="plu-s twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="'.$row['ss_share_go'].'" target="_blank" class="plu-s google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
          </td>
        </tr>';
    }
    $recorrido++;
    $i++;
  }
  $data = [];
  $data['datos'] = $html;
  $data['total'] = 10;

  echo json_encode( $data );
  wp_die();
}

/*---- ajax estadistica autores */
add_action( 'wp_ajax_autores_ajax', 'autores_ajax_init' );
add_action( 'wp_ajax_nopriv_autores_ajax', 'autores_ajax_init' );

function autores_ajax_init(){

  $args = array( 'orderby' => 'display_name', 'order' => 'ASC', 'who' => 'authors' );
  $authors = get_users( $args );

  $autores = '';

  $totalpost = [];
  $autores_lis = [];
  $colores =[];
    $autores = '';
  for ( $i = 0; $i < count( $authors ); $i++ ) {
    $author = $authors[$i];
    $query = array( 'author' => $author->ID, 'posts_per_page' => -1 );
    $posts = count(query_posts($query));
    $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    $autores .= '<div class="plus-flex fila-autor">
        <span style="padding-bottom: 5px;border-bottom:2px solid '.$color.';">'.$author->display_name.'</span>
        <span class="total-share">'.$posts.' post</span>
    </div>';
    $totalpost[$i] =$posts;
    $autores_lis[$i] = $author->display_name;
    $colores[$i] = $color;
  }

  wp_send_json([
		'response' => true,
		'autores' => $autores,
    'valores' => $totalpost,
    'autores_lis' => $autores_lis,
    'colores' => $colores
	]);

  wp_die();

}

/*---- ajax estadistica autores */
add_action( 'wp_ajax_exportarResultado_ajax', 'exportarResultado_ajax_init' );
add_action( 'wp_ajax_nopriv_exportarResultado_ajax', 'exportarResultado_ajax_init' );

if (!function_exists('exportarResultado_ajax_init')) {
	function exportarResultado_ajax_init() {

			# Load slim WP
			define( 'WP_USE_THEMES', false );
			require_once( ABSPATH . 'wp-load.php' );
      require_once  'lib/funciones.php';
      require_once  'lib/ofd-share.php';
      require_once  'lib/PHPExcel.php';

			$objPHPExcel = new PHPExcel();

			$objPHPExcel->setActiveSheetIndex(0);
			// Initialise the Excel row number
			$rowCount = 0;
			// Sheet cells
			$cell_definition = array(
				'A' => 'id',
				'B' => 'titulo',
				'C' => 'autor',
				'D' => 'fecha',
				'E' => 'totaltipo',
        'F' => 'link'
			);



      $fechainicial = explode('-',$_GET['fi']);
      $fechafinal   = explode('-',$_GET['ff']);
      $autor        = $_GET['author'];
      $redes        = $_GET['redes'];
      $cantidad     = $_GET['cantidad'];
      $tipo         = $_GET['tipo'];
      $cat          = $_GET['cat'];

      require_once  'consulta.php';



			//Build headers
			foreach( $cell_definition as $column => $value )
				$objPHPExcel->getActiveSheet()->setCellValue( "{$column}1", $value );
			// Build cells
			while( $rowCount <= $cantidad ){

				$cell = $rowCount + 2;
				foreach( $cell_definition as $column => $value )
					$objPHPExcel->getActiveSheet()->setCellValue($column.$cell, $filas[$rowCount][$value]);

			    $rowCount++;
			}

			// Redirect output to a client’s web browser
				ob_end_clean();
				ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="item_list.xlsx"');
        header('Cache-Control: max-age=0');
				$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        ob_end_clean();
        ob_start();
        $objWriter->save("php://output");

        $xlsData = ob_get_contents();
        ob_end_clean();
        $response =  array(
                'op' => 'ok',
                'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
            );

        echo json_encode($response);
        wp_die();

	}
}



function page_index(){
?>

<div class="plu-wrap">
    <section class="plus-flex plu-s-header">
        <div class="plu-s-page-header">
          <h1><?php _e('Social Share Total','social-share-total');?></h1>
        </div>
    </section>

    <section class="plus-flex contenedor">
        <div class="item filtro">
          <div class="header">
              <span><?php _e('FILTRAR RESULTADOS','social-share-total');?></span>
          </div>

          <div class="lista-filtros">

            <form  name="data_filtro" id="data_filtro" method="post">

              <div class="plus-flex filtros">
                  <span><?php _e('Fecha Inicial','social-share-total');?></span>
                  <input type="text" name="fi" id="fi" class="icon-fecha" placeholder="&#xf073;">
              </div>

              <div class="plus-flex filtros">
                  <span><?php _e('Fecha Final','social-share-total');?></span>
                  <input type="text" name="ff" id="ff" class="icon-fecha" placeholder="&#xf073;">
              </div>

              <div class="plus-flex filtros">
                  <span><?php _e('Autores','social-share-total');?></span>
                  <?php wp_dropdown_users(array( 'show_option_none'=>'Todos','name' => 'author')); ?>
              </div>

              <div class="plus-flex filtros">
                  <span><?php _e('Categoria','social-share-total');?></span>
                  <?php wp_dropdown_categories(array( 'show_option_none'=>'Todos','name' => 'cat')); ?>
              </div>

              <div class="plus-flex filtros">
                  <span><?php _e('Buscar por','social-share-total');?></span>
                  <select name="tipo" id="tipo">
                    <option value="compartido"><?php _e('Compartidos','social-share-total');?></option>
                    <option value="visto"><?php _e('Vistos','social-share-total');?></option>
                  </select>
              </div>

              <div class="plus-flex filtros">
                  <span><?php _e('Redes sociales','social-share-total');?></span>
                  <select name="redes" id="redes">
                    <option value=""><?php _e('Todos','social-share-total');?></option>
                    <option value="facebook"><?php _e('Facebook','social-share-total');?></option>
                    <option value="twitter"><?php _e('Twitter','social-share-total');?></option>
                  </select>
              </div>

              <div class="plus-flex filtros">
                  <span><?php _e('Cantidad de post','social-share-total');?></span>
                  <select name="cantidad" id="cantidad">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option>
                    <option value="45">45</option>
                    <option value="50">50</option>
                  </select>
              </div>

              <button class="plus-bton button-small bton-filtro" type="button"><?php _e('REALIZAR BUSQUEDA','social-share-total');?></button>
              <div class="spinner-plu">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
              </div>
            </form>

          </div>
    </div>

    <div class="item resultados">

        <div class="item caja">
          <div class="header">
              <span><?php _e('RESULTADOS','social-share-total');?></span>
              <button class="plus-bton button-small bton-exportar" type="button">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span><?php _e('EXPORTAR RESULTADOS','social-share-total');?></span>
              </button>
          </div>

          <div class="filtr-resultado">
              <table class="plu-table">
                <thead>
                  <tr>
                    <th class="t-head"><?php _e('Titulo','social-share-total');?></th>
                    <th class="t-head" id="texto-tipo"><?php _e('Total','social-share-total');?></th>
                    <th class="t-head"><?php _e('Compartir','social-share-total');?></th>
                  </tr>
              </thead>
              <tbody id="resultados">

                <tr>
                  <td class="c-body">



              </tbody>
            </table>
          </div>

        </div>

        <div id="seccion-autores" class="item caja">

          <div class="spinner-plu" style="display:block !important;">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
          </div>

          <div class="header">
              <span><?php _e('RENDIMIENTO DE AUTORES','social-share-total');?></span>
          </div>

          <div class="plus-flex autores-caja">



              <div class="flex lista_autores">
              </div>

              <div class="grafica_autores">
                <div id="canvas-holder">
                   <canvas id="chart-area">
               </div>

              </div>


          </div>

        </div>



    </div>




    </section>

  </div>


<?php
}
