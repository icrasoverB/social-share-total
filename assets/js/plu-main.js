
jQuery(function(){


  jQuery( "#fi" ).datepicker({
      dateFormat : "yy-mm-dd"
  });

  jQuery( "#ff" ).datepicker({
      dateFormat : "yy-mm-dd"
  });


  jQuery(document).on('click', '.bton-filtro', function (event) {
		event.stopPropagation();
		event.preventDefault();

		jQuery.ajax({
			url: plu_admin_ajax,
			type: 'GET',
      dataType: 'json',
			data: {
				'action': 'filtro_ajax',
				'ff': jQuery('#ff').val(),
        'fi': jQuery('#fi').val(),
        'author': jQuery('#author').val(),
        'tipo': jQuery('#tipo').val(),
        'redes': jQuery('#redes').val(),
        'cantidad': jQuery('#cantidad').val(),
        'cat': jQuery('#cat').val()
			},
			beforeSend: function () {
        jQuery('.bton-filtro').prop('disabled', true);
        jQuery('.spinner-plu').css({'display':'block'});
        jQuery('#resultados').html('');
        jQuery('.bton-exportar').hide();
			},
			success: function (data) {
        jQuery('#resultados').html(data.datos);
        jQuery('.bton-filtro').prop('disabled', false);
        jQuery('.spinner-plu').css({'display':'none'});
        jQuery('#texto-tipo').html(jQuery('#tipo').val());
        jQuery('.bton-exportar').show();
        window.myDoughnut.update();
			},
			error: function (jqXHR, textStatus, errorThrown) {
        jQuery('.bton-filtro').prop('disabled', false);
        jQuery('.spinner-plu').css({'display':'none'});

			}
		});
});

    jQuery(document).on('click', '.bton-exportar', function (event) {
  		event.stopPropagation();
  		event.preventDefault();

  		jQuery.ajax({
  			url: plu_admin_ajax,
  			type: 'GET',
        dataType: 'json',
  			data: {
  				'action': 'exportarResultado_ajax',
  				'ff': jQuery('#ff').val(),
          'fi': jQuery('#fi').val(),
          'author': jQuery('#author').val(),
          'tipo': jQuery('#tipo').val(),
          'redes': jQuery('#redes').val(),
          'cantidad': jQuery('#cantidad').val(),
          'cat': jQuery('#cat').val()
  			},
  			beforeSend: function () {
          jQuery('.bton-exportar span').html('CREANDO ARCHIVO...');
          jQuery('.bton-exportar .fa').css({'opacity':'1'});
  			},
  			success: function (data) {
          jQuery('.bton-exportar .fa').css({'opacity':'0'});
          jQuery('.bton-exportar span').html('EXPORTAR RESULTADOS');
          var $a = jQuery("<a>");
          $a.attr("href",data.file);
          jQuery("body").append($a);
          $a.attr("download","Resultados.xls");
          $a[0].click();
          $a.remove();
  			},
  			error: function (jqXHR, textStatus, errorThrown) {
          jQuery('.bton-exportar span').html('EXPORTAR RESULTADOS');
          jQuery('.bton-exportar .fa').css({'opacity':'0'});
  			}
  		});

	});



});



    window.onload = function() {

      if(jQuery('.plu-wrap').length){

        jQuery.ajax({
    			url: plu_admin_ajax,
    			type: 'GET',
    			data: {
    				'action': 'autores_ajax'
    			},
    			beforeSend: function () {
            jQuery('#seccion-autores').addClass('carga');
            jQuery('#seccion-autores .spinner-plu').css({'display':'block'});
    			},
    			success: function (data) {
            if(data.response){
              jQuery('.lista_autores').html(data.autores);


              var config = {
                      type: 'doughnut',
                      data: {
                          datasets: [{
                              data: data.valores,
                              backgroundColor:data.colores,
                          }],
                          labels: data.autores_lis
                      },
                      options: {
                          responsive: true,
                          legend: {
                              display: false,
                              position: 'top',

                          },
                          title: {
                              display: false,
                              text: 'Autores'
                          },
                          animation: {
                              animateScale: true,
                              animateRotate: true
                          }
                      }
                  };

              var ctx = document.getElementById("chart-area").getContext("2d");
              window.myDoughnut = new Chart(ctx, config);
              jQuery('#seccion-autores').removeClass('carga');
              jQuery('#seccion-autores .spinner-plu').css({'display':'none'});
            }
    			},
    			error: function (jqXHR, textStatus, errorThrown) {

    			}
    		});

      }

    };
