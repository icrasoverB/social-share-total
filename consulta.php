<?php

$args = array(
  'posts_per_page' => -1,
  'date_query' => array(
    array(
      'after'     => array(
        'year'  => $fechainicial[0],
        'month' => $fechainicial[1],
        'day'   => $fechainicial[2],
      ),
      'before'    => array(
        'year'  => $fechafinal[0],
        'month' => $fechafinal[1],
        'day'   => $fechafinal[2],
      ),
      'inclusive' => true
    )));

if($autor!='-1'):
  $args['author'] = $autor;
endif;


if($cat!='-1'):
  $args['cat'] = $cat;
endif;


  $query = new WP_Query ($args);
  $lista = [];
  $filas = [];
  $i = 0;
  $j =2;

if ($query->have_posts()) : while ($query->have_posts()) :
      $query->the_post();
      $totaltipo = 0;
      if($tipo=='visto'){
        $count_key = '_mts_view_count';
        $count = get_post_meta(get_the_id(), $count_key, true);
        $totaltipo = $count;
      }
      if($tipo=='compartido'){
        if(!empty($redes)){
          $count_key = 'mashsb_jsonshares';
          $count = get_post_meta(get_the_id(), $count_key, true);
          $array = json_decode($count);
          if($redes=='facebook'){
              $totaltipo = $array->facebook_shares;
          }
          if($redes=="twitter"){
              $totaltipo = $array->twitter;
          }
          //$totaltipo = 11;
        }
        else{
          /*$s_f = share_count(get_the_id(), 'facebook');
          $s_t = share_count(get_the_id(), 'twitter');
          $s_g = share_count(get_the_id(), 'google-plus');
          $social_f = $s_f > 0 ? $s_f:0;
          $social_t = $s_t > 0 ? $s_t:0;
          $social_g = $s_g > 0 ? $s_g:0;
          $totaltipo = $social_f + $social_t +$social_g;*/
          $count_key = 'mashsb_shares';
          $count = get_post_meta(get_the_id(), $count_key, true);
          $totaltipo = $count;
        }
      }


      global $post;
      $post = get_post(get_the_id());

      $ss_share_fb = SocialShare_generateSocialUrls(['facebook'], [
          'url' => get_the_permalink()
        ], true);
      $ss_share_tw = SocialShare_generateSocialUrls(['twitter'], [
          'url' => get_the_permalink()
        ], true);

      $ss_share_go = SocialShare_generateSocialUrls(['google'], [
            'url' => get_the_permalink()
        ], true);

      $filas[$i] = array(
          'id' => get_the_id(),
          'titulo' => get_the_title(),
          'autor' => get_the_author(),
          'fecha' => get_the_date(),
          'totaltipo' => $totaltipo,
          'link' => get_the_permalink(),
          'ss_share_fb' => $ss_share_fb['url'],
          'ss_share_tw' => $ss_share_tw['url'],
          'ss_share_go' => $ss_share_go['url'],
          'img' => get_the_post_thumbnail_url(get_the_id(), 'medium')
        );

      $i++;
      $j++;
    endwhile;
    wp_reset_query();

endif;

foreach ($filas as $key => $row) {
    $aux[$key] = $row['totaltipo'];
}
array_multisort($aux, SORT_DESC, $filas);

?>
